#!/bin/bash
XPLUA_V2_PATH=$HOME/work/xplua_v2_env/stage

export LD_LIBRARY_PATH
LD_LIBRARY_PATH=$XPLUA_V2_PATH/lib:$LD_LIBRARY_PATH

export LUA_INIT="package.path='$XPLUA_V2_PATH/lua/?.lua;$XPLUA_V2_PATH/lua/?.lc;$HOME/local/share/lua/5.1/?.lua;$HOME/local/share/lua/5.1/?.lc'..package.path ; package.cpath='$XPLUA_V2_PATH/lua/?.so;$HOME/local/lib/lua/5.1/?.so;'..package.cpath;$LUA_INIT" 

lua "$@" <&0
