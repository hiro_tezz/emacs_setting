;;package$B4XO"(B
(require 'package)
;; MELPA$B$rDI2C(B
(add-to-list 'package-archives '("melpa" . "http://melpa.org/packages/"))
(add-to-list 'package-archives '("melpa-stable" . "http://stable.melpa.org/packages/"))
;; $B=i4|2=(B
(package-initialize)

;; $B%+%9%?%^%$%:MQ$N%U%!%$%k$r@_Dj(B
(setq custom-file "~/.emacs.d/custom_setttings.el")
(load custom-file t)

;;$B3F<o%Q%9$N@_Dj(B
;;$B4D6-JQ?t(BMY_EMACS_SETTING_PATH$B$r@_Dj$7$J$$$3$H$K$h$j%Q%9$N(B
;;$B@_Dj$r@)8f(B
(setq my-emacs-setting-path
      (or (getenv "MY_EMACS_SETTING_PATH") "~/emacs_setting"))
(add-to-list 'load-path "/usr/local/share/emacs/site-lisp")
(add-to-list 'load-path (concat my-emacs-setting-path "/lisp"))

;;$B?'@_Dj(B
(require 'color-theme)
(color-theme-initialize)
(color-theme-ld-dark)

;$B?'$,8+$E$i$$$N$G=$@5(B
(add-hook 'ediff-load-hook
	  (lambda ()
	    (set-face-foreground 'ediff-current-diff-B "magenta")
	    (set-face-foreground 'ediff-even-diff-A "blue")
	    (set-face-foreground 'ediff-even-diff-B "blue")
	    (set-face-foreground 'ediff-even-diff-C "blue")
	    (set-face-foreground 'ediff-odd-diff-A "black")
	    (set-face-foreground 'ediff-odd-diff-B "black")
	    (set-face-foreground 'ediff-odd-diff-C "black")))

;;$BF|K\8l@_Dj(B
(set-default-coding-systems 'utf-8) 

;; magit$B@_Dj(B
(when (require 'magit nil 'noerror)
  (setq magit-last-seen-setup-instructions "1.4.0")
  (define-key global-map "\C-x\C-e" 'magit-status)
  (set-face-foreground 'magit-log-graph "magenta")
  (setenv "GIT_PAGER" "cat"))

;; $B%Q%9%o!<%IF~NO$N1#JC(B
(setq comint-password-prompt-regexp
      "\\(\\(Enter \\|[Oo]ld \\|[Nn]ew \\|'s \\|login \\|Kerberos \\|CVS \\|UNIX \\| SMB \\|LDAP \\|\\[sudo] \\|^\\)[Pp]assword\\( (again)\\)?\\|pass phrase\\|\\(Enter \\|Repeat \\|Bad \\)?[Pp]assphrase\\|$B%Q%9%o!<%I(B\\|[Pp]assword\\)\\(?:, try again\\)?\\(?: for [^:]+\\)?:\\s *\\'")

;; $B%P%C%U%!0lMwA`:n(B
(when (require 'elscreen nil 'noerror)
  (setq elscreen-prefix-key (kbd "C-x C-z"))
  (elscreen-start)
  (define-key elscreen-map (kbd "C-d") 'elscreen-kill)
  (define-key global-map (kbd "C-x <right>") 'elscreen-next)
  (define-key global-map (kbd "C-x <right>") 'elscreen-next)
  (define-key global-map (kbd "C-x <left>") 'elscreen-previous))

;; ido$B4X78(B
(ido-mode t)
(ido-everywhere 1)

(require 'ido)
;Alt-RET$B$G%U%)%k%@%*!<%W%s!&?75,%U%!%$%k:n@.!JJd40$7$J$$$G!K(B
(add-hook 'ido-setup-hook
		  (lambda() 
			(define-key ido-completion-map (kbd "M-RET") 'ido-select-text)))
;TAB$B$G8uJd$,0l$D$@$1$G$b>!<j$K(Bopen$B$7$J$$(B
(setq ido-confirm-unique-completion 1)
;$BBgJ8;z>.J8;z$rH=JL$7$FJd40(B
(setq ido-case-fold nil)
;$B$*$;$C$+$$$J8!:w!J2a5n$NF1L>%U%!%$%k$r>!<j$KA*Br!K$r$J$/$9(B
(setq ido-auto-merge-work-directories-length -1)

;; ibuffer$B4X78(B
(define-key global-map (kbd "C-x C-b") 'ibuffer)
;ibuffer$BFb$G(Bido-find-file$B$,8F$P$l$J$$LdBj$NBP=h(B
(defun ibuffer-ido-find-file (file &optional wildcards)
  "Like `ido-find-file', but default to the directory of the buffer at point."
  (interactive
   (let ((default-directory
		   (let ((buf (ibuffer-current-buffer)))
			 (if (buffer-live-p buf)
				 (with-current-buffer buf
				   default-directory)
			   default-directory))))
	 (list (ido-read-file-name "Find file: " default-directory) t)))
  (find-file file wildcards))
(add-hook 'ibuffer-mode-hook
		  (lambda ()
			(define-key ibuffer-mode-map (kbd "C-x C-f")
			  'ibuffer-ido-find-file)))

;;$B%+!<%=%k7O%-!<%P%$%s%I(B
(define-key global-map (kbd "C-x <up>") (lambda (arg) (interactive "p") (other-window (- arg))))
(define-key global-map (kbd "C-x <down>") 'other-window)
(define-key global-map (kbd "C-x <insertchar>") 'split-window-below)
(define-key global-map (kbd "C-x <deletechar>") 'delete-window)
(define-key global-map (kbd "C-x <home>") 'delete-other-windows)
(define-key global-map (kbd "C-x <end>") 'ido-switch-buffer)
(define-key global-map (kbd "C-x <prior>") 'previous-buffer)
(define-key global-map (kbd "C-x <next>") 'next-buffer)

(define-key global-map (kbd "<M-up>") 'backward-up-list)
(define-key global-map (kbd "<M-down>") 'backward-down-list)
(define-key global-map (kbd "<M-right>") 'forward-sexp)
(define-key global-map (kbd "<M-left>") 'backward-sexp)

(define-key global-map (kbd "<insertchar>") 'yank)
(define-key global-map (kbd "<M-insertchar>")
  (lambda() (interactive)
	(if (eq last-command 'yank)
		(yank-pop)
	  (yank))))
(define-key global-map (kbd "<M-deletechar>")
  (lambda() (interactive)
	(if (use-region-p)
		(kill-region (region-beginning) (region-end))
	  (kill-line))))

(define-key global-map (kbd "<M-home>") 'recenter-top-bottom)
(define-key global-map (kbd "<M-end>") 'ido-switch-buffer)

(define-key global-map (kbd "<M-prior>") 'beginning-of-buffer)
(define-key global-map (kbd "<M-next>") 'end-of-buffer)

;;comint-mode(shell/gud/$B!&!&!&(B)
(add-hook 'comint-mode-hook
		  (lambda ()
			(local-set-key (kbd "M-b") 'comint-previous-prompt)
			(local-set-key (kbd "M-f") 'comint-next-prompt)
			(local-set-key (kbd "<M-left>") 'comint-previous-prompt)
			(local-set-key (kbd "<M-right>") 'comint-next-prompt)
			(local-set-key (kbd "<M-up>") 'comint-previous-input)
			(local-set-key (kbd "<M-down>") 'comint-next-input)))

;;undo-redo
(require 'redo)
(define-key global-map [?\C-z] 'undo)
(define-key global-map [?\C-\M-z] 'redo)

;;
(setq default-tab-width 4)

;; lua$B4XO"(B
;(setq lua-default-application "xplua")
;(setq lua-prompt-regexp "^:[^>]*>+[\t ]+")
(setq lua-default-application (concat my-emacs-setting-path "/execlua.sh"))
(load-library "lua-mode")
(add-hook 'lua-mode-hook
          (lambda ()
			 (define-key lua-mode-map (kbd "C-c C-k") 'lua-restart-with-whole-file)
			 (setq indent-tabs-mode nil)))

;; c-mode
(defconst my-c-style
  '((c-basic-offset . 4)
    (setq tab-width 4)
    (c-offsets-alist . ((arglist-intro . +)
                        (arglist-close . 0)
			(substatement-open . 0)))))

(defun my-c-style-init ()
	     (setq indent-tabs-mode nil)
	     (setq tab-width 4)
	     (c-add-style "my-c-style" my-c-style t)
             (c-set-style "my-c-style"))

(add-hook 'c-mode-common-hook 'my-c-style-init)
(add-hook 'c++-mode-common-hook 'my-c-style-init)

;;tag-search 
(defadvice tags-search (before default-value activate compile)
  "Provide default values."
  (interactive
   (list
    (let ((default (funcall
                    (or find-tag-default-function
                        (get major-mode 'find-tag-default-function)
                        'find-tag-default))))
      (read-string
       (format "Tags search (default %s): " default)
       nil nil default t))))
  (ring-insert find-tag-marker-ring (point-marker)))
(global-set-key "\M-+" 'tags-search)

;;$B$=$NB>$b$m$b$m(B
(require 'cmake-mode nil 'noerror)

;$BJG:G8e$K<+F02~9T$r9T$o$J$$(B
(setq mode-require-final-newline nil)

;ediff$B$N@)8f%Q%M%k$r(Bwindow$B$K$7$J$$(B
(setq ediff-window-setup-function 'ediff-setup-windows-plain)

;$B9TKv2~9T$,L5$$$H(Bediff$B$,<:GT$9$kLdBj$NBP:v(B
(setq ediff-diff-program (concat my-emacs-setting-path "/mydiff.sh"))

(show-paren-mode t) ;$B3g8L6(D4I=<((B
(menu-bar-mode 0) ;$B%a%K%e!<$$$i$J$$(B
(setq-default make-backup-files nil) ;$B%P%C%/%"%C%W%U%!%$%k(B(~)$B$O$D$/$i$J$$(B

;lua-mode$BEy$G(Bemacs$B%P!<%8%g%s$NLdBj$G(Bcalled-interactively-p$B$N0z?t$G%(%i!<$H$J$k0Y(B
(fset 'org-called-interactively-p (symbol-function 'called-interactively-p))
(cond ((< emacs-major-version 24)
       (defun called-interactively-p (&optional arg)
	 (org-called-interactively-p))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; for putty
(define-key key-translation-map (kbd "ESC <insertchar>") (kbd "<M-insertchar>"))
(define-key key-translation-map (kbd "ESC <deletechar>") (kbd "<M-deletechar>"))
(define-key key-translation-map (kbd "ESC <home>") (kbd "<M-home>"))
(define-key key-translation-map (kbd "<select>") (kbd "<end>"))
(define-key key-translation-map (kbd "ESC <select>") (kbd "<M-end>"))
(define-key key-translation-map (kbd "C-x <select>") (kbd "C-x <end>"))
(define-key key-translation-map (kbd "ESC <prior>") (kbd "<M-prior>"))
(define-key key-translation-map (kbd "ESC <next>") (kbd "<M-next>"))
(define-key key-translation-map (kbd "ESC <up>") (kbd "<M-up>"))
(define-key key-translation-map (kbd "ESC <left>") (kbd "<M-left>"))
(define-key key-translation-map (kbd "ESC <down>") (kbd "<M-down>"))
(define-key key-translation-map (kbd "ESC <right>") (kbd "<M-right>"))
